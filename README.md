# Script d'installation des machines Naova [V5 ONLY]

Ce script bash a pour objectif d'installer et configurer les stations de travail des membres du club étudiant, et se destine particulièrement aux nouveaux membres.

> Testé et approuvé sur UBUNTU 18.04.1 LTS

### Les tâches effectuées par le script sont :

  - Update du système
  - Installation de git
  - Définition du répertoire par défaut "~/naova"
  - Paramétrage de git
  - Clonage du repo gitlab du club
  - Installation des librairies
  - Téléchargement du SDK Naoqi, depuis une machine virtuelle appartenant à Benjamin Caron à l'adresse http://23.95.228.106/
  - Décompression du SDK
  - Compilation du code
  
  

### Exécution

  - S'assurer d'avoir les droits d'exécution en faisant un 
  > chmod 700 installation.sh
  - Exécuter le script
 > ./installation.sh

