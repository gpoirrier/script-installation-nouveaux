#!/bin/bash   
#
# Script for naova installation
# Author: Guillaume Poirrier
#
echo "WELCOME TO NAOVA !"
sudo apt update -y
sudo apt install -y git
echo "Code will be stored in \"~/naova\" directory by default"
NAOVA_ROOT=~/naova
read -p "User Git mail :"  usermail
git config --global user.email "$usermail"
read -p "Naova Gitlab username :" username
git config --global user.name "$username"
if ! [ -f $NAOVA_ROOT ]
then
	git clone https://gitlab.com/ClubNaova/NaovaCode.git/ $NAOVA_ROOT
fi
echo "Installing libraries..."
sudo apt-get install -y clang-4.0 qtbase5-dev libqt5svg5-dev libglew-dev libxml2-dev graphviz xterm make
echo "Downloading Naoqi SDK..."
cd $NAOVA_ROOT
wget http://23.95.228.106/naoqi-sdk-2.1.4.13-linux32.tar.gz
echo "Decompressing NaoQi while executing Alcommon..."
./Install/installAlcommon naoqi-sdk-2.1.4.13-linux32.tar.gz
rm -rf naoqi-sdk-2.1.4.13-linux32.tar.gz
while true; do
    read -p "There we go ! ready to build the project ? [Y/n]" yn
    case $yn in
        [Yy]* ) echo "V--COMPILING--V"
		cd $NAOVA_ROOT/Make/Linux;
		make;
		break
		;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done 

